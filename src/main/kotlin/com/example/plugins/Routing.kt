package com.example.plugins

import com.example.model.movieStorage
import com.example.route.commentRouting
import com.example.route.movieRouting
import io.ktor.server.routing.*
import io.ktor.server.response.*
import io.ktor.server.application.*

fun Application.configureRouting() {
    routing {
        movieRouting()
        commentRouting()
    }
}
