package com.example.model

import kotlinx.serialization.Serializable
import java.time.LocalDateTime
import java.time.LocalTime

@Serializable
data class Comentari(
    var id: String,
    var idPelicula: String,
    var comentario: String,
    var fechaDeCreacion: String
){
    init {
        fechaDeCreacion = LocalDateTime.now().toString()
    }
}



