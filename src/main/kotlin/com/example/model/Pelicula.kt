package com.example.model

import kotlinx.serialization.Serializable

@Serializable
data class Pelicula(
    var id: String,
    var titulo: String,
    var anyo: String,
    var genero: String,
    var director: String,
    var comentari: MutableList<Comentari>
)

val movieStorage = mutableListOf<Pelicula>()