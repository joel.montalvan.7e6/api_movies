package com.example.route

import com.example.model.Comentari
import com.example.model.Pelicula
import com.example.model.commentStorage
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.commentRouting() {
    route("/comments") {
        get {
            if (commentStorage.isNotEmpty()) call.respond(commentStorage)
            else call.respondText ("No comments found.", status = HttpStatusCode.OK)
        }
        get("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@get call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )

            val id = call.parameters["id"]
            for (comment in commentStorage){
                if (comment.id == id) return@get call.respond(comment)
            }
            call.respondText(
                "Customer with id $id not found",
                status = HttpStatusCode.NotFound
            )
        }

        post {
            val comment = call.receive<Comentari>()
            commentStorage.add(comment)
            call.respondText("Customer stored correctly", status = HttpStatusCode.Created)
        }

        delete("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@delete call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            for (comment in commentStorage) {
                if (commentStorage.removeIf { it.id == id }) {
                    call.respondText("Comment removed correctly", status = HttpStatusCode.Accepted)
                }
            }
            call.respondText(
                "Not Found",
                status = HttpStatusCode.NotFound
            )
        }

        put("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@put call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            val custToUpd = call.receive<Comentari>()
            for (comment in commentStorage) {
                if (comment.id == id) {
                    comment.idPelicula = custToUpd.idPelicula
                    comment.comentario= custToUpd.comentario
                    comment.fechaDeCreacion = custToUpd.fechaDeCreacion
                    return@put call.respondText(
                        "Comment with id $id has been updated",
                        status = HttpStatusCode.Accepted
                    )
                }
            }
            call.respondText(
                "Not Found",
                status = HttpStatusCode.NotFound
            )
        }
    }
}

