package com.example.route

import com.example.model.Pelicula
import com.example.model.movieStorage
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.movieRouting() {
    route("/movies") {
        get {
            if (movieStorage.isNotEmpty()) call.respond(movieStorage)
            else call.respondText ("No movies found.", status = HttpStatusCode.OK)
        }
        get("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@get call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )

            val id = call.parameters["id"]
            for (movie in movieStorage){
                if (movie.id== id) return@get call.respond(movie)
            }
            call.respondText(
                "Movie with id $id not found",
                status = HttpStatusCode.NotFound
            )
        }

        post {
            val pelicula = call.receive<Pelicula>()
            movieStorage.add(pelicula)
            call.respondText("Movie stored correctly", status = HttpStatusCode.Created)
        }

        delete("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@delete call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            for (movie in movieStorage) {
                if (movieStorage.removeIf { it.id == id }) {
                    call.respondText("Movie removed correctly", status = HttpStatusCode.Accepted)
                }
            }
            call.respondText(
                "Movie with id $id not found",
                status = HttpStatusCode.NotFound
            )
        }

        put("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@put call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            val custToUpd = call.receive<Pelicula>()
            for (movie in movieStorage) {
                if (movie.id == id) {
                    movie.titulo = custToUpd.titulo
                    movie.anyo= custToUpd.anyo
                    movie.genero = custToUpd.genero
                    movie.director = custToUpd.director
                    return@put call.respondText(
                        "Customer with id $id has been updated",
                        status = HttpStatusCode.Accepted
                    )
                }
            }
            call.respondText(
                "Movie with tittle $id not found",
                status = HttpStatusCode.NotFound
            )
        }

        get("{id?/comments}") {
            if (call.parameters["id"].isNullOrBlank()) return@get call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )

            val id = call.parameters["id"]
            for (movie in movieStorage){
                if (movie.id== id) return@get call.respond(movie.comentari)
            }
            call.respondText(
                "Movie with id $id not found",
                status = HttpStatusCode.NotFound
            )
        }

    }

}

